# Cours de pédagogie
## 1 Penser concevoir élaborer
### 1.1 Références
[Cours funMooc ](https://lms.fun-mooc.fr/courses/course-v1:inria+41027+session01/courseware/806fd60e97ab4b84b7ee89643b440dc4/da10d222633045dfb70b688c967a68ef/)
### 1.2 Intro
* La **question primordiale** est celle des fondamentaux, c'est à dire bien **partager des notions fondamentales au delà des outils disponibles à un instant donné**.  
* Le **deuxième enjeu fondamental** est celui de l'**adaptation des contenus** aux élèves, à la fois **collectivement** et **individuellement**  
### 1.3 Concept et connaissance
**Problème :**  
la connaissance des concepts et la connaissance des produits jouent toutes deux un rôle essentiel dans le domaine de l'informatique. 
Si trop de concept élève manque de pratique
* Si **trop de concept** élève **manque de pratique** et ne pourrons pas mettre **leurs connaissances en application**
* Si **trop** d'importance à la transmission de la connaissance des **produits**, l'enseignement ne sera **pas pérenne**.  Les étudiants ne pourront alors pas **adapter** et appliquer **leurs connaissances** à des situations nouvelles. 
**Classifier une connaissance :**  
|Connaissance des produits|Connaissance des concepts|
|:-----:|:-----:|
|Liée au produit|Indépendante du produit|
|Court terme|Long terme|
|Apprentissage par cœur, restitution|Comprendre et organiser|
|Faits isolés|Relations|
|Peu de transfert possible|Transfert possible|
|Concret|Abstrait|

**Solution :**  
L'**utilisation avantageuse** des moyens informatiques est conditionnée **à la fois** par la **connaissance des produits** et par la **connaissance des concepts**. Un enseignement peut être considéré comme **particulièrement efficace** lorsque les enseignants sont **systématiquement** en mesure d'établir la **relation entre le concept et le produit**. 
### 1.4 Adapter le contenu
**Problème** :  
L'enseignement de l'informatique s'adresse à des **publics variés** dont les **connaissances préalables** et les **besoins** sont **différents**. Celui qui enseigne à des publics différents court le risque de commettre des **erreurs dans son choix des thèmes principaux** du cours et d'illustrer ses sujets par des **exemples non pertinents** pour les étudiants présents  

**Solution** :  
La **connaissance** préalable des besoins du public visé est une **condition indispensable** à un **enseignement informatique de qualité**. C'est ce même **public visé** qui **déterminera** le niveau d'**approfondissement des connaissances** sur les concepts et les produits. 
### 1.5 Idées fondamentales
**Problème** :  
L'**informatique évolue** à une vitesse fulgurante, de **nouvelles technologies** et de nouveaux **produits** voient continuellement le jour.  Dans l'enseignement de l'informatique, le **risque** de se laisser **trop influencer** par cette évolution est **élevé**, une influence qui peut même amener à **perdre de vue** les véritables **fondamentaux de la formation**. 

**Le développement très rapide des technologies et des produits dans le secteur de l'informatique a plusieurs conséquences sur la formation** :  
  * de nouvelles sessions de formation :  Les besoins changeants de l'économie conduisent à de nouveaux cours de formation, qui deviennent souvent caducs 
  * supports de cours d'informatique :Le secteur de l'informatique n'est pas très attrayant pour les éditeurs de manuels d'enseignement, car les programmes changent fréquemment et de coûteuses mises à jour sont nécessaires. 
  * charge des professeurs d'informatique : il faut régulièrement adapter les documents de cours, les fiches d'exercice et les présentations aux dernières versions présentes sur le marché. 
  * période de validité des connaissances :Les connaissances acquises par les étudiants durant les cours d'informatique deviennent souvent inutiles peu de temps après. L'accent n'est pas mis sur des concepts pérennes, mais plutôt sur la connaissance des produits et des tâches associées. Beaucoup d'étudiants n'ont pas conscience qu'en informatique aussi, il existe des connaissances et des concepts durables. 

**Les idées fondamentales de Bruner :**  
Une idée fondamentale est un fait qui répond aux critères suivant :**
|Critère|Description|
|-----:|:-----|
|critère **horizontal**|peut être utilisé ou reconnu sous de nombreuses formes dans différents domaines.|
|critère **vertical**|peut être démontré et communiqué à chaque niveau intellectuel.|
|critère de **temps**|est clairement perceptible dans l'évolution historique et reste pertinent à long terme.|
|critère de **sens**|est en rapport avec le langage et la pensée au quotidien du monde réel.|
|critère de **représentation**|peut être représenté à différents niveaux cognitifs => énactif (le geste), iconique (l’image du geste), symbolique (explication verbale du geste).|

**Un enseignement qui s'appuie sur des idées fondamentales garantit le choix d'un contenu pérenne.**  
* **Qu'apportent les idées fondamentales à l'enseignement ?**
  * Apporte une aide précieuse dans la préparation des leçons :
    * Obligé de réfléchir sur la signification d'un fait :
    * Pourquoi la substance est-elle importante ?
    * Pourquoi les élèves doivent-ils la comprendre ? 
  * Le contrôle du critère de représentation donne lieu à une présentation intuitive du fait :
    * le critère horizontal couvre les analogies dans la vie quotidienne
    * le critère vertical mène à des explications facilement compréhensibles.
  * Les réflexions relatives aux idées fondamentales aident également l'enseignant à structurer les documents du cours.  
    * en contenus pérennes 
    * en contenus dont la durée de vie est plus courte (Ces derniers doivent être actualisés plus fréquemment ) 
  * Pour les élèves, les idées fondamentales simplifient la compréhension de faits complexes :
    * Les faits peuvent être intégrés plus facilement dans un ensemble plus grand 
    * Les connaissances acquises précédemment et réellement comprises peuvent plus facilement être transposées à des situations nouvelles. 
  * On évite le risque de se perdre dans des détails éphémères**, **mais ne suffit pas** suivant le public visé, les **concepts fondamentaux d'un sujet** peuvent être extrêmement **pertinents ou dénués de toute importance suivant le public :  
_Les formes normalisées des bases de données relationnelles présentent peu d'intérêt si la formation sur les bases de données s'adresse aux utilisateurs, mais sont primordiales pour les futurs développeurs._

**Solution** :  
La **formation informatique doit s'articuler** autour de **contenus pérennes, les concepts fondamentaux et les méthodes**. **Le principe des idées fondamentales** représente un instrument qui **permet de vérifier le caractère significatif d'un sujet ou d'un fait**.  
**Cette vérification fournit des indices** importants pour la **préparation de la substance et l'organisation du cours**.
Contrairement aux matières traditionnelles, l'informatique en tant que sujet d'enseignement est encore mal établie. Les enseignants ne peuvent généralement pas s'en remettre à une liste exhaustive d'idées fondamentales et n'ont souvent pas d'autre choix que d'identifier eux-mêmes celles qui seront adaptées au public visé dans un domaine précis.

**Références** :  
Dans ce contexte des idées fondamentales, nous recommandons la consultation de l'ouvrage  
* _The New Turing Omnibus: Sixty-Six Excursions In Computer Science par Dewdney [Dew01]. Ce livre présente des approches simples et motivantes pour différent sujets. Il est à la fois un recueil d'exemples et une source d'idées. Un autre livre à recommander est Computers Ltd.: What They Really Can't Do de David Harel [Har01], une présentation résumée des sujets fondamentaux de l'informatique théorique, de la calculabilité et de la théorie de la complexité._
* _Nous citerons ci-après sans aucune prétention à l'exhaustivité un certain nombre d'idées fondamentales, avec une description détaillée des trois premières._
