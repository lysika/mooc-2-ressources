# Partie 1
## Sous-partie 1 : texte
Une phrase sans rien  
_Une phrase en italique_  
**Une phrase en gras**  
[Un lien vers fun-mooc.fr](https://www.fun-mooc.fr/fr/)  

Une ligne de ```code```

## Sous-partie 2 : listes

Liste à puce
* iteml
  * sous-itemm
  * sous-itemm
* iteml
* iteml  

Liste numérotée
1. item1.
2. item2.
3. item3.


## Sous-partie 3 : code
``` 
Extrait de code
```
