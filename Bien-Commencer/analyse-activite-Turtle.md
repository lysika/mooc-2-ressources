## Analyse activité Turtle

* Rappel Taxonomie de Bloom :
  1. définir, nommer, citer, énumérer ...
  2. décrire, résumer, expliquer, interpréter ...
  3. utiliser, résoudre, construire, démontrer, calculer,dériver...
  4. analyser, distinguer, comparer, choisir...
  5. concevoir, rédiger, planifier, réaliser, faire un exposé, produire, mettre au point, ...
  6. justifier, défendre, juger, argumenter, critiquer,évaluer, ... 

* Rappel intérogation d'une activité
quels objectifs en terme de connaissances et de compétences visées ; 
  * quel positionnement de l'activité dans la progressivité annuelle, quel(s) pré-requis ; 
  * quels exercices "cibles" de l'activité au regard des objectifs de celle-ci ; 
  * quelle durée pour l'activité ; 
  * quel déroulement prévu pour l'activité ; 
  * comment anticiper les démarches que les élèves vont mettre en place ; 
  * comment anticiper les difficultés que vont rencontrer les élèves ; 
  * quel étayage pour lever ces difficultés ; 
  * comment gérer l'hétérogénéité de cette activité. 
* **Objectifs**
  * Initiation à Python mais surtout aux algorithmes
  * Utiliser des lignes de commandes Python
  * Appeler des méthodes simple
  * Décrire des signature simples : nom de la méthode et paramètre
  * Utiliser diviser pour mieux réigner
  * Concevoir des méthodes
* **Pré-requis à cette activité**
  * Utiliser interpreteur Python
  * Traduire des mots simples en anglais
  * PC pour chaque élève ou en groupe
  * Vérifier que turtle est accéssible en import
  * Algo simple
* **Durée de l'activité**
  * 1h00 à 2h00
* **Exercices cibles**
  * Concvoir une première méthode
* **Description du déroulement de l'activité**
  1. Faire des groupes
  2. Se connecter à l'envirronnement
  3. Distribuer la feuille
  4. Expliquer rapidement le voulu => faire un algorithme, diviser pour mieux régner
  5. Faire le tour de groupes pour aider, donner des les documentations de turtle
* **Anticipation des difficultés des élèves**
  * Trouver de la documentation : turtle, boucle ...
  * Difficulté à créer la 1ère méthode => Envoyer quelqu'un ou bien le faire au tableau car pas important à ce moment là
  * Difficulté à acceder à 
* **Gestion de l'hétérogénéïté**
  * Les exercices sont de plus en plus dure
  * si déborde aide les collègues
